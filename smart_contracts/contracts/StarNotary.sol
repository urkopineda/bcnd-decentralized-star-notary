pragma solidity ^0.4.23;

import "./ERC721Token.sol";

contract StarNotary is ERC721Token {

    struct Star {
        string name;
        string starStory;
        string ra;
        string dec;
        string mag;
    }

    struct Existence {
        bool exists;
    }

    mapping(uint256 => Star) public tokenIdToStarInfo;
    mapping(bytes32 => Existence) public existingStars;
    mapping(uint256 => uint256) public starsForSale;

    function createStar(string _name, string _starStory, string _ra, string _dec, string _ma, uint256 _tokenId) public { 
        Star memory newStar = Star(_name, _starStory, _ra, _dec, _ma);
        Existence memory newExistence = Existence(true);
        bytes32 starHash = keccak256(abi.encodePacked(_ra, _dec, _ma));
        require(!existingStars[starHash].exists, "Star already registered");
        tokenIdToStarInfo[_tokenId] = newStar;
        existingStars[starHash] = newExistence;
        ERC721Token.mint(msg.sender, _tokenId);
    }

    function putStarUpForSale(uint256 _tokenId, uint256 _price) public { 
        require(this.ownerOf(_tokenId) == msg.sender, "You are not the owner of this star");
        starsForSale[_tokenId] = _price;
    }

    function buyStar(uint256 _tokenId) public payable { 
        require(starsForSale[_tokenId] > 0, "This star is not on sale");
        uint256 starCost = starsForSale[_tokenId];
        address starOwner = this.ownerOf(_tokenId);
        require(msg.value >= starCost, "The provided value is not enought for buying this star");
        ERC721Token.removeTokenFrom(starOwner, _tokenId);
        ERC721Token.addTokenTo(msg.sender, _tokenId);
        starOwner.transfer(starCost);
        if (msg.value > starCost) { 
            msg.sender.transfer(msg.value - starCost);
        }
    }

    function checkIfStarExist(uint256 _tokenId) public view returns (bool) {
        if (bytes(tokenIdToStarInfo[_tokenId].name).length != 0) {
            return true;
        }
        return false;
    }

}