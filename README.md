# Blockchain Nanodegree - Decentralized Star Notary

This project contains a smart contract written in Solidity to run a star notary system. This smart contract includes a front end to create stars and obtain their information and it's deployed [in this link](https://urkopineda.gitlab.io/bcnd-decentralized-star-notary) using GitLab Pages.

## RESULTS

Results:
- Wallet address `0xeC7a8c3815F30548B2F401EaCD3A3e0835507EbC`
- Transaction hash for the new star: `0xcc88b48d23968a6fd23bf365571842ef133f6213fbfd191b47c622e8461bd8fa`
- Star token ID: `1`
- Transaction hash for the contract creation: `0x93aae2544ff6148afc50b6db3be646a013269e671e204e7897d95f4f0cd7fab5`
- Contract address: `0xCB55354E54d2149fEcc4B8DC1abf0bFD2F329D74`

## Testing

Use the command `truffle test`, you will need to deploy a local network using, for example, Ganache.

## Compile

To compile the smart contract, use `truffle compile`

## Deploy to the network

Deploy the contract using `truffle migrate --network rinkeby`
